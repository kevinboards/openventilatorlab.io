

## Who We Are

We are a worldwide group of over 100 (and growing!) designers, hardware engineers, software engineers, scientists, lawyers, and manufacturers. We are lucky to be supported by some great companies leading the way in helping us with our project (OSH Park PCB Manufacturing, KiCad EDA and even SolidWorks CAD software). 

<blockquote class="imgur-embed-pub" lang="en" data-id="88TIniS"><a href="//imgur.com/88TIniS">View post on imgur.com</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

![fRender](images/frontRender.JPG)



## Our Goal

Our goal is to support the production of professional grade ventilators worldwide. These differ from many open source ventilators in that they will provide precise, automated control over mechanical and environmental settings required to properly regulate a person’s breathing. Furthermore, they are based on a known and tested design - the Medtronic PB560. This project aims to provide the necessary information required to make safe and reliable ventilators for use in a medical setting anywhere in the world.

![backRender](images/backRender2.JPG)

## The Challenge

As the COVID-19 pandemic continues to spread across the globe, there is a growing demand for a wide variety of medical equipment. One of the most notable examples is the increased demand for ventilators for those who fall critically ill and require breathing support. This has caused many companies to step up in incredible and inspiring ways. Among those companies is Medtronic who have released the design files for the PB560 ventilator. These design files serve as our baseline for a safe, reliable, and proven system.

Unfortunately, the original Medtronic PB560 files were created using a variety of commercial software packages. This limits their accessibility worldwide. Furthermore, the design was originally done in the mid 2000s. These issues ultimately mean two things. 

The files need to be updated to use the same tool for each circuit board design. 
Some of the components in the design are either obsolete or end of life. So alternative solutions must be identified and sourced.

![circuit1](images/circuit1.jpg)

As the COVID-19 pandemic spreads it also has a major impact on global manufacturing and supply chains. The surge in demand for a wide range of products is impossible for manufacturers to keep up with. It also makes it difficult to source components to begin ramping up production to try to meet the surge in demand.

Perhaps the most important motivation for having a proven design is that the FDA approval process is understandably stringent. Especially when compared to many other standards which govern different types of electronic systems. To start from scratch on something as complicated as a ventilator could take years from beginning the design to obtaining FDA approval. Additionally, any changes to an already approved product must be documented and validated. This only further exacerbates the issues previously discussed above.


## Why Open Source?

From circuit boards to CAD files it is part of our mission to make all information openly available on software platforms that are accessible to anyone around the globe. Additionally, we can leverage the open source community to gain access to a wide variety of expertise that might not be available in a more traditional setting.

![circuit2](images/circuit2b.jpg)